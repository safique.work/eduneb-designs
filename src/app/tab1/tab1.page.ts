import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
    programs = [
      {
        image: 'assets/home/study1.png',
        title: 'IIT JEE Advance',
        content: '12 Chapters'
      },
      {
        image: 'assets/home/study1.png',
        title: 'IIT JEE Advance',
        content: '12 Chapters'
      },
      {
        image: 'assets/home/study1.png',
        title: 'IIT JEE Advance',
        content: '12 Chapters'
      },
      {
        image: 'assets/home/study1.png',
        title: 'IIT JEE Advance',
        content: '12 Chapters'
      }
    ];
    videos = [
       {
         image: 'assets/home/video-image.png',
         title: 'Lecture-1',
         content: 'Electrostatics'
       },
      {
        image: 'assets/home/video-image.png',
        title: 'Lecture-1',
        content: 'Electrostatics'
      },
      {
        image: 'assets/home/video-image.png',
        title: 'Lecture-1',
        content: 'Electrostatics'
      },
      {
        image: 'assets/home/video-image.png',
        title: 'Lecture-1',
        content: 'Electrostatics'
      }
      ];
    blogs = [
         {
           image: 'assets/home/blog.png',
           title: 'SSC CGL Tier 3 Admit Card 2019',
         },
      {
        image: 'assets/home/blog.png',
        title: 'SSC CGL Tier 3 Admit Card 2019',
      },
      {
        image: 'assets/home/blog.png',
        title: 'SSC CGL Tier 3 Admit Card 2019',
      },
      {
        image: 'assets/home/blog.png',
        title: 'SSC CGL Tier 3 Admit Card 2019',
      }

      ];

  constructor(private router: Router) {}
  ngOnInit() {

  }
  goToSubjects(subject) {
    // console.log(subject);
    this.router.navigateByUrl('subject');
  }

}
