import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{
  forumTabs = [
    {
      title: 'All',
      active: true
    },
    {
      title: 'Education',
      active: false
    },
    {
      title: 'Entertainment',
      active: false
    },
    {
      title: 'Education',
      active: false
    }
  ];
  forums = [
    {
      image: 'assets/community/img.jpg',
      title: 'Black Manta',
      subtitle: 'News',
      date: '12 June 2018',
      content: 'It is a long established fact that a reader will be distracted by ' +
          'the readable content of a page when looking at its layout.',
      commentImage: 'assets/community/comment-image.jpg',
    },
    {
      image: 'assets/community/img.jpg',
      title: 'Black Manta',
      subtitle: 'News',
      date: '12 June 2018',
      content: 'It is a long established fact that a reader will be distracted ' +
          'by the readable content of a page when looking at its layout.',
      commentImage: 'assets/community/comment-image.jpg',
    },
  ];
  constructor() { }
  ngOnInit() {
  }
  activateForum(forumTab) {
    this.forumTabs.forEach(item => item.active = false);
    forumTab.active = true;
  }
}
