import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'subject',
    loadChildren: () => import('./pages/course/subject/subject.module').then( m => m.SubjectPageModule)
  },
  {
    path: 'chapter',
    loadChildren: () => import('./pages/course/chapter/chapter.module').then( m => m.ChapterPageModule)
  },
  {
    path: 'bookmarks',
    loadChildren: () => import('./pages/bookmarks/bookmarks.module').then( m => m.BookmarksPageModule)
  },
  {
    path: 'topic',
    loadChildren: () => import('./pages/course/topic/topic.module').then( m => m.TopicPageModule)
  },
  {
    path: 'tab4',
    loadChildren: () => import('./tab4/tab4.module').then( m => m.Tab4PageModule)
  },
  {
    path: 'sepcialscourse',
    loadChildren: () => import('./pages/course/sepcialscourse/sepcialscourse.module').then( m => m.SepcialscoursePageModule)
  },
  {
    path: 'buycourse',
    loadChildren: () => import('./pages/course/buycourse/buycourse.module').then( m => m.BuycoursePageModule)
  },
  {
    path: 'testlist',
    loadChildren: () => import('./pages/test/testlist/testlist.module').then( m => m.TestlistPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./pages/notification/notification.module').then( m => m.NotificationPageModule)
  },
  {
    path: 'contribution',
    loadChildren: () => import('./pages/contribution/contribution.module').then( m => m.ContributionPageModule)
  },
  {
    path: 'testmaterial',
    loadChildren: () => import('./pages/test/testmaterial/testmaterial.module').then( m => m.TestmaterialPageModule)
  },
  {
    path: 'testimonial',
    loadChildren: () => import('./pages/testimonial/testimonial.module').then( m => m.TestimonialPageModule)
  },
  {
    path: 'wallet',
    loadChildren: () => import('./pages/wallet/wallet.module').then( m => m.WalletPageModule)
  },
  {
    path: 'purchase',
    loadChildren: () => import('./pages/purchase/purchase.module').then( m => m.PurchasePageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/auth/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'otp',
    loadChildren: () => import('./pages/auth/otp/otp.module').then( m => m.OtpPageModule)
  },
  {
    path: 'user-info',
    loadChildren: () => import('./pages/auth/user-info/user-info.module').then( m => m.UserInfoPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/auth/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
