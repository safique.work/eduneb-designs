import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  sidePages = [
    {
      title: 'Home',
      image: 'home',
      url: 'tabs/tab1'
    },
    {
      title: 'Notifications',
      image: 'notifications-outline',
      url: 'notification'
    },
    {
      title: 'Bookmarks',
      image: 'bookmark',
      url: 'bookmarks'
    },
    {
      title: 'Our Courses',
      image: 'star',
      url: 'tabs/'
    },
    {
      title: 'My Wallet',
      image: 'wallet',
      url: 'wallet'
    },
    {
      title: 'Purchased Items',
      image: 'share',
      url: 'purchase'
    },
    {
      title: 'About us',
      image: 'alert',
      url: 'tabs'
    },
    {
      title: 'Contact us',
      image: 'call',
      url: 'tabs'
    },
    {
      title: 'T & C',
      image: 'checkmark-circle',
      url: 'tabs'
    },
    {
      title: 'Log Out',
      image: 'log-out',
      url: 'tabs'
    }

  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  goTo(item) {
    console.log(item);
    this.router.navigateByUrl(item.url);
  }
}
