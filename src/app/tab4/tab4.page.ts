import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  activeTab = 'myInfo';
  profiles = [
    {
      image: 'assets/icon/avatar.png',
      title: 'Hello John!',
      content1: 'Student Of Arjun Classes',
      content2: 'Since 03 Years'
    }
  ];
  constructor(private router: Router) { }

  ngOnInit() {
  }
  changeTab(data) {
    this.activeTab = data;
  }
  goToContribution() {
    this.router.navigateByUrl('contribution');
  }
}
