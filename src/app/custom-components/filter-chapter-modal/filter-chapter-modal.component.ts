import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-filter-chapter-modal',
  templateUrl: './filter-chapter-modal.component.html',
  styleUrls: ['./filter-chapter-modal.component.scss'],
})
export class FilterChapterModalComponent implements OnInit {

  constructor(private modalController: ModalController) { }

  ngOnInit() {}
  dismiss() {
    this.modalController.dismiss();
  }
}
