import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FilterChapterModalComponent} from './filter-chapter-modal/filter-chapter-modal.component';
import {TestInstructionComponent} from './test-instruction/test-instruction.component';


@NgModule({
  declarations: [FilterChapterModalComponent,
  TestInstructionComponent],
  imports: [
    CommonModule,
      IonicModule
  ],
  exports: [],
  entryComponents: [
      FilterChapterModalComponent,
      TestInstructionComponent
  ]
})
export class CustomComponentsModule { }
