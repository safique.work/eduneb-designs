import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestmaterialPage } from './testmaterial.page';

const routes: Routes = [
  {
    path: '',
    component: TestmaterialPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestmaterialPageRoutingModule {}
