import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TestmaterialPage } from './testmaterial.page';

describe('TestmaterialPage', () => {
  let component: TestmaterialPage;
  let fixture: ComponentFixture<TestmaterialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestmaterialPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TestmaterialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
