import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testmaterial',
  templateUrl: './testmaterial.page.html',
  styleUrls: ['./testmaterial.page.scss'],
})
export class TestmaterialPage implements OnInit {
  activeTab = 'video';
  videos = [
    {
      image: 'assets/icon/play-button.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/play-button.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/play-button.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/play-button.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
  ];
  studies = [
    {
      image: 'assets/icon/writing1.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/writing1.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/writing1.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/writing1.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
  ];
  questions = [
    {
      image: 'assets/icon/writing1.png',
      title: 'What is the net force and its direction that the charges at...',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/writing1.png',
      title: 'What is the net force and its direction that the charges at...',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/writing1.png',
      title: 'What is the net force and its direction that the charges at...',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/writing1.png',
      title: 'What is the net force and its direction that the charges at...',
      subtitle: 'Electrostatics'
    },
  ];
  dpps = [
    {
      image: 'assets/icon/writing1.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/writing1.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/writing1.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
    {
      image: 'assets/icon/writing1.png',
      title: 'Lecture-1',
      subtitle: 'Electrostatics'
    },
  ];
  constructor() { }

  ngOnInit() {
  }
  changeTab(data) {
    this.activeTab = data;
  }

}
