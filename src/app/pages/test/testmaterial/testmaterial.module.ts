import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestmaterialPageRoutingModule } from './testmaterial-routing.module';

import { TestmaterialPage } from './testmaterial.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestmaterialPageRoutingModule
  ],
  declarations: [TestmaterialPage]
})
export class TestmaterialPageModule {}
