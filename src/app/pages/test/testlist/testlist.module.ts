import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestlistPageRoutingModule } from './testlist-routing.module';

import { TestlistPage } from './testlist.page';
import {CustomComponentsModule} from '../../../custom-components/custom-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestlistPageRoutingModule,
      CustomComponentsModule
  ],
  declarations: [TestlistPage]
})
export class TestlistPageModule {}
