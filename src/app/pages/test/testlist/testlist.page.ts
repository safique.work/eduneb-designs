import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {TestInstructionComponent} from '../../../custom-components/test-instruction/test-instruction.component';

@Component({
  selector: 'app-testlist',
  templateUrl: './testlist.page.html',
  styleUrls: ['./testlist.page.scss'],
})
export class TestlistPage implements OnInit {
  activeTab = 'testlist';
  testlists = [
    {
      test: 19,
      questions: 180,
      hours: 3,
      marks: 720,
      isPurchased: true
    },
    {
      test: 18,
      questions: 180,
      hours: 3,
      marks: 720,
      isPurchased: true
    },
    {
      test: 17,
      questions: 180,
      hours: 3,
      marks: 720,
      isPurchased: false
    },
    {
      test: 16,
      questions: 180,
      hours: 3,
      marks: 720,
      isPurchased: false
    },
    {
      test: 15,
      questions: 180,
      hours: 3,
      marks: 720,
      isPurchased: false
    }
  ];
  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }
  changeTab(data) {
    this.activeTab = data;
  }
  async showInstructions() {
    const modal = await this.modalController.create({
      component: TestInstructionComponent,
      cssClass: 'test-instruction-css',
      componentProps: {}
    });
    return await modal.present();
  }
}
