import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  passwordIcon = 'eye-off';
  constructor(private router: Router) { }

  ngOnInit() {
  }
  goToSignUp() {
    this.router.navigateByUrl('signup');
  }
  goToForgotPassword() {
    this.router.navigateByUrl('forgot-password');
  }

}
