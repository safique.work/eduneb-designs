import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.page.html',
  styleUrls: ['./chapter.page.scss'],
})
export class ChapterPage implements OnInit {
  activeTab = 'chapter';
  chapters = [
      {
          title: 'Electrostatics',
      },
      {
          title: 'Magnetic Effects of Current',
      },
      {
          title: 'Laws of Thermodynamics',
      },
      {
          title: 'Laws of Thermodynamics',
      },
      {
          title: 'Rotational Motion',
      }
  ];
  special = [
      {
          type: 'Premium',
          image: 'assets/courses/chapter-image.jpg',
          title: 'Fundamentals of Mathematics',
          subimage: 'assets/icon/avatar.png',
          subtitle: 'By Arjun Thori',
          prograss: '0.5',
      },
      {
          type: 'Premium',
          image: 'assets/courses/chapter-image.jpg',
          title: 'Fundamentals of Mathematics',
          subimage: 'assets/icon/avatar.png',
          subtitle: 'By Arjun Thori',
          prograss: '0.7',
      },
      {
          type: 'Premium',
          image: 'assets/courses/chapter-image.jpg',
          title: 'Fundamentals of Mathematics',
          subimage: 'assets/icon/avatar.png',
          subtitle: 'By Arjun Thori',
          prograss: '0.7',
      },
  ];
  constructor(private router: Router) { }

  ngOnInit() {
  }
   changeTab(data) {
    this.activeTab = data;
 }
 goToTopic(data) {
      this.router.navigateByUrl( 'topic');
 }
 goToCourses(data) {
      this.router.navigateByUrl('sepcialscourse');
 }
}
