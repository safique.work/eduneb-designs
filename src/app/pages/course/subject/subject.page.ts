import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.page.html',
  styleUrls: ['./subject.page.scss'],
})
export class SubjectPage implements OnInit {
  activeTab = 'subject';
  subjects = [
    {
      title: 'Physics',
      image: 'assets/icon/atom1.png',
    },
    {
      title: 'Chemistry',
      image: 'assets/icon/atom1.png',
    },
    {
      title: 'Mathematics',
      image: 'assets/icon/atom1.png',
    },
  ];
  tests = [
    {
      image: 'assets/home/study1.png',
      title: 'IIT JEE Main Test Series',
      content: '12 Chapters'
    },
    {
      image: 'assets/home/study1.png',
      title: 'IIT JEE Main Test Series',
      content: '12 Chapters'
    },
    {
      image: 'assets/home/study1.png',
      title: 'IIT JEE Main Test Series',
      content: '12 Chapters'
    },
    {
      image: 'assets/home/study1.png',
      title: 'Make My Test',
      content: 'Custom Test'
    }
  ];
  stores = [
    {
      image: 'assets/icon/course1.png',
      title: 'Calculus Course (Full)',
      price: 449,
    },
    {
      image: 'assets/icon/course1.png',
      title: 'Calculus Course (Full)',
      price: 449,
    },
    {
      image: 'assets/icon/course1.png',
      title: 'Calculus Course (Full)',
      price: 449,
    },
    {
      image: 'assets/icon/course1.png',
      title: 'Calculus Course (Full)',
      price: 449,
    },
    {
      image: 'assets/icon/course1.png',
      title: 'Calculus Course (Full)',
      price: 449,
    }
  ];
  constructor(private router: Router) { }

  ngOnInit() {
  }
  changeTab(data) {
    this.activeTab = data;
  }
  goToChapter(data) {
    this.router.navigateByUrl('chapter');
  }
  goToTest(data) {
    this.router.navigateByUrl('testlist');
  }
}
