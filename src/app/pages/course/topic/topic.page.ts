import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.page.html',
  styleUrls: ['./topic.page.scss'],
})
export class TopicPage implements OnInit {
  contents = [
    {
      title: 'Check differentiability using differentiation',
      isPurchased: true
    },
    {
      title: 'Diffrence between derivate and limiting value of slope at a time',
      isPurchased: true
    },
    {
      title: 'Diffrence between derivate and limiting value of slope at a time',
      isPurchased: false
    },
    {
      title: 'Diffrence between derivate and limiting value of slope at a time',
      isPurchased: false
    }
  ];
  topics = [
    {
      image: 'assets/icon/exercise.png',
      title: 'Videos',
      active: true
    },
    {
      image: 'assets/icon/exercise.png',
      title: 'Excercise',
      active: false
    },
    {
      image: 'assets/icon/exercise.png',
      title: 'Ebook',
      active: false
    }
  ];
  constructor() { }

  ngOnInit() {
  }
  activateTopic(topic) {
    this.topics.forEach(item => item.active = false);
    topic.active = true;
  }
}
