import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuycoursePage } from './buycourse.page';

describe('BuycoursePage', () => {
  let component: BuycoursePage;
  let fixture: ComponentFixture<BuycoursePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuycoursePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuycoursePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
