import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuycoursePageRoutingModule } from './buycourse-routing.module';

import { BuycoursePage } from './buycourse.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuycoursePageRoutingModule
  ],
  declarations: [BuycoursePage]
})
export class BuycoursePageModule {}
