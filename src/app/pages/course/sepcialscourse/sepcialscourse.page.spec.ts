import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SepcialscoursePage } from './sepcialscourse.page';

describe('SepcialscoursePage', () => {
  let component: SepcialscoursePage;
  let fixture: ComponentFixture<SepcialscoursePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SepcialscoursePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SepcialscoursePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
