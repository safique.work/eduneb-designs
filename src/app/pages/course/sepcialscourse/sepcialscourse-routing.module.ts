import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SepcialscoursePage } from './sepcialscourse.page';

const routes: Routes = [
  {
    path: '',
    component: SepcialscoursePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SepcialscoursePageRoutingModule {}
