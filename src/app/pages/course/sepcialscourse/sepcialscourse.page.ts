import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sepcialscourse',
  templateUrl: './sepcialscourse.page.html',
  styleUrls: ['./sepcialscourse.page.scss'],
})
export class SepcialscoursePage implements OnInit {
  buyCard = {};
  contents = [
    {
      title: 'Check differentiability using differentiation',
      isPurchased: true
    },
    {
      title: 'Diffrence between derivate and limiting value of slope at a time',
      isPurchased: true
    },
    {
      title: 'Diffrence between derivate and limiting value of slope at a time',
      isPurchased: false
    },
    {
      title: 'Diffrence between derivate and limiting value of slope at a time',
      isPurchased: false
    }
  ];
  topics = [
    {
      image: 'assets/icon/exercise.png',
      title: 'Videos',
      active: true
    },
    {
      image: 'assets/icon/exercise.png',
      title: 'Excercise',
      active: false
    },
    {
      image: 'assets/icon/exercise.png',
      title: 'Ebook',
      active: false
    }
  ];
  purchases = [
    {
      noMonth: 1,
      month: 'Months',
      price: 399,
      isActive: false
    },
    {
      noMonth: 3,
      month: 'Months',
      price: 759,
      isActive: false
    },
    {
      noMonth: 6,
      month: 'Months',
      price: 1999,
      isActive: false
    }
  ];
  activeSection = 'course';
  constructor() { }

  ngOnInit() {
  }
  activateTopic(topic) {
    this.topics.forEach(item => item.active = false);
    topic.active = true;
  }
  buyClick() {
    this.activeSection = 'purchase';
  }
  setCard(card) {
    if (card.isActive === false) {
      this.purchases.forEach((item) => item.isActive = false);
      card.isActive = true;
      this.buyCard = card;
    } else {
      card.isActive = false;
      this.buyCard = {};
    }
  }
  isEmpty(obj) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
}
