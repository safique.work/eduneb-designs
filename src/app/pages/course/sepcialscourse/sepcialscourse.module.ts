import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SepcialscoursePageRoutingModule } from './sepcialscourse-routing.module';

import { SepcialscoursePage } from './sepcialscourse.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SepcialscoursePageRoutingModule
  ],
  declarations: [SepcialscoursePage]
})
export class SepcialscoursePageModule {}
