import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.page.html',
  styleUrls: ['./purchase.page.scss'],
})
export class PurchasePage implements OnInit {
  purchases = [
    {
      image: 'assets/icon/test.png',
      title: 'IIT - JEE Mains Test Series',
      image2: 'assets/icon/cart.png',
    },
    {
      image: 'assets/icon/test.png',
      title: 'IIT - JEE Mains Test Series',
      image2: 'assets/icon/cart.png',
    },
    {
      image: 'assets/icon/study1.png',
      title: 'IIT - JEE Mains Test Series',
      image2: 'assets/icon/cart.png',
    },
    {
      image: 'assets/icon/study1.png',
      title: 'IIT - JEE Mains Test Series',
      image2: 'assets/icon/cart.png',
    },
    {
      image: 'assets/icon/study1.png',
      title: 'IIT - JEE Mains Test Series',
      image2: 'assets/icon/cart.png',
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
