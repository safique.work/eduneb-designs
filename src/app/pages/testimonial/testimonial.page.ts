import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.page.html',
  styleUrls: ['./testimonial.page.scss'],
})
export class TestimonialPage implements OnInit {
  testimonials = [
      {
        image: 'assets/testimonial/image1.png',
        title: 'Cameron Dean',
        content: 'It is a long established fact that a reader will be... ',
      },
    {
      image: 'assets/testimonial/image1.png',
      title: 'Cameron Dean',
      content: 'It is a long established fact that a reader will be... ',
    },
    {
      image: 'assets/testimonial/image1.png',
      title: 'Cameron Dean',
      content: 'It is a long established fact that a reader will be... ',
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
