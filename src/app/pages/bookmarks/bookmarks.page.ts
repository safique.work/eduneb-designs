import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.page.html',
  styleUrls: ['./bookmarks.page.scss'],
})
export class BookmarksPage implements OnInit {
  bookmarks = [
    {
     image: 'assets/icon/physics.svg',
     title: 'Physics',
     video: 8,
     question: 3,
    },
    {
      image: 'assets/icon/physics.svg',
      title: 'Chemistry',
      video: 8,
      question: 3,
    },
    {
      image: 'assets/icon/physics.svg',
      title: 'Mathematics',
      video: 8,
      question: 3,
    },
    {
      image: 'assets/icon/physics.svg',
      title: 'Biology',
      video: 8,
      question: 3,
    },
  ];
  constructor(private router: Router) { }

  ngOnInit() {
  }
  goToInner(data) {
    this.router.navigateByUrl('bookmarks/bookmarksinner');
  }
}
