import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookmarksPage } from './bookmarks.page';

const routes: Routes = [
  {
    path: '',
    component: BookmarksPage
  },
  {
    path: 'bookmarksinner',
    loadChildren: () => import('./bookmarksinner/bookmarksinner.module').then( m => m.BookmarksinnerPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookmarksPageRoutingModule {}
