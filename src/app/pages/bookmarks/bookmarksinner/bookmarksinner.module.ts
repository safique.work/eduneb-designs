import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookmarksinnerPageRoutingModule } from './bookmarksinner-routing.module';

import { BookmarksinnerPage } from './bookmarksinner.page';
import {FilterChapterModalComponent} from '../../../custom-components/filter-chapter-modal/filter-chapter-modal.component';
import {CustomComponentsModule} from '../../../custom-components/custom-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookmarksinnerPageRoutingModule,
      CustomComponentsModule
  ],
  declarations: [BookmarksinnerPage],
})
export class BookmarksinnerPageModule {}
