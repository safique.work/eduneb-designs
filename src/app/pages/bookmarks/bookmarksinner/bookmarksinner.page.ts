import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FilterChapterModalComponent} from '../../../custom-components/filter-chapter-modal/filter-chapter-modal.component';

@Component({
  selector: 'app-bookmarksinner',
  templateUrl: './bookmarksinner.page.html',
  styleUrls: ['./bookmarksinner.page.scss'],
})
export class BookmarksinnerPage implements OnInit {
  activeTab = 'questions';
  questions = [
    {
      title: 'Which of the following statement is true regarding the muscles',
      subtitle: 'Units and measurement',
    },
    {
      title: 'Which of the following statement is true regarding the muscles',
      subtitle: 'Units and measurement',
    },
    {
      title: 'Which of the following statement is true regarding the muscles',
      subtitle: 'Units and measurement',
    },
    {
      title: 'Which of the following statement is true regarding the muscles',
      subtitle: 'Units and measurement',
    },
    {
      title: 'Which of the following statement is true regarding the muscles',
      subtitle: 'Units and measurement',
    },
  ];
  videos = [
    {
      image: 'assets/bookmarks/video-image.jpg',
      title: 'Physics Relation with other branches of science',
      content: '11.40 mins',
    },
    {
      image: 'assets/bookmarks/video-image.jpg',
      title: 'Physics Relation with other branches of science',
      content: '11.40 mins',
    },
    {
      image: 'assets/bookmarks/video-image.jpg',
      title: 'Physics Relation with other branches of science',
      content: '11.40 mins',
    },
    {
      image: 'assets/bookmarks/video-image.jpg',
      title: 'Physics Relation with other branches of science',
      content: '11.40 mins',
    }
  ];
  constructor(private modalController: ModalController) { }
  changeTab(data) {
    this.activeTab = data;
  }
  ngOnInit() {
  }
  async openFilter() {
    const modal = await this.modalController.create({
      component: FilterChapterModalComponent,
      cssClass: 'filter-chapter-modal-css',
      componentProps: {}
    });
    return  await modal.present();
  }
}
