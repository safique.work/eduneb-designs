import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookmarksinnerPage } from './bookmarksinner.page';

describe('BookmarksinnerPage', () => {
  let component: BookmarksinnerPage;
  let fixture: ComponentFixture<BookmarksinnerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookmarksinnerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookmarksinnerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
