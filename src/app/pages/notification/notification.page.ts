import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
  notifications = [
    {
      image: 'assets/icon/notification.png',
      title: 'Pre Meter Launched',
      content: 'Now Prepare yourself in Preparation meter.',
      isRead: false,
    },
    {
      image: 'assets/icon/notification.png',
      title: 'Pre Meter Launched',
      content: 'Now Prepare yourself in Preparation meter.',
      isRead: true,
    },
    {
      image: 'assets/icon/notification.png',
      title: 'Pre Meter Launched',
      content: 'Now Prepare yourself in Preparation meter.',
      isRead: true,
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
