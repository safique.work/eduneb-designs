import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {
  points = [
    {
      image: 'assets/icon/point-icon.png',
      title: 'Total Points',
      price: 299,
    },
    {
      image: 'assets/icon/point-icon.png',
      title: 'Earned Points',
      price: 199,
    },
  ];
  earnPoints = [
    {
      image: 'assets/icon/point-icon.png',
      title: 'Invite Friends',
      content: 'Earn upto 200 points / friend when the invite friends\n' +
          'complete the tasks',
      point: '200',
    },
    {
      image: 'assets/icon/point-icon.png',
      title: 'Invite Friends',
      content: 'Earn upto 200 points / friend when the invite friends\n' +
          'complete the tasks',
      point: '300',
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
