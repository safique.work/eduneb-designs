import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-contribution',
  templateUrl: './contribution.page.html',
  styleUrls: ['./contribution.page.scss'],
})
export class ContributionPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
